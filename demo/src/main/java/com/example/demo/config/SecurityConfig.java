package com.example.demo.config;

import org.springframework.boot.autoconfigure.info.ProjectInfoProperties.Build;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;

import lombok.AllArgsConstructor;

// class de config @Configuration
// Active le web secu, permet de modifier les comptes utilisateurs @EnableWebSecurity
// Active les anotation de securité @EnableMethodSecurity(securedEnabled = true)
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
@AllArgsConstructor // tout les constructeurs
public class SecurityConfig {

    // utilisateur en memoire, sans BD
    // Definition des roles
    // #############################Donnée en dur###########################
    //@Bean
    // InMemoryUserDetailsManager users(){
    //     return  new InMemoryUserDetailsManager(
    //         // Definition des roles
    //         User.builder().username("admin").password("{noop}admin").roles("ADMIN", "USER").build(),  //noop = no op password encoder pas de cryptage
    //         User.builder().username("user").password("{noop}user").roles("USER").build()
    //     );
    // }

    // Definie les droits des roles
    private UserDetailsService userDetailsService;
    // config clef public / private
    private RsaKeyProperties keyProperties;

    //UNIQUEMENT POUR TEST
    // @Bean
    // NoOpPasswordEncoder encoder(){
    //     return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    // }

    @Bean
    PasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    AuthenticationManager authenticationManager(HttpSecurity httpSecurity) throws Exception{
        return httpSecurity.getSharedObject(AuthenticationManagerBuilder.class)
            .userDetailsService(userDetailsService)
            .passwordEncoder(encoder())
            .and()
            .build();
    }

    // 
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
        return http
        .csrf().disable() // le csrf JUSTE pour les tests avec Postman, à enlever en prod
            .authorizeHttpRequests(auth -> auth
                // .requestMatchers(HttpMethod.GET, "/personnes").hasRole("USER")
                .requestMatchers(HttpMethod.POST, "/token").permitAll()
                .anyRequest().authenticated()
            )
            // Auth basic
            .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
            .httpBasic(Customizer.withDefaults())
            .build();
    }

    // Encode le jeton
    @Bean
    JwtDecoder jwtDecoder(){
        return NimbusJwtDecoder.withPublicKey(keyProperties.publicKey()).build();
    }

    // Decode le jeton
    @Bean
    JwtEncoder jwtEncoder(){
        JWK jwk = new RSAKey.Builder(keyProperties.publicKey()).privateKey(keyProperties.privateKey()).build();
        JWKSource<SecurityContext> jwks = new ImmutableJWKSet<>(new JWKSet(jwk));
        return new NimbusJwtEncoder(jwks);
    }
}
