package com.example.demo.controller;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UserDto;
import com.example.demo.service.JwtService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@AllArgsConstructor
@Slf4j

public class JwtController {
    
    private JwtService jwtService;
    private AuthenticationManager authenticationManager;

    @PostMapping("/token")
    public Map<String, String> getToken(@RequestBody UserDto user){
        if (user.getGrantType().equals("password")) {
            Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword())
            );
            String role = jwtService.getRoles(authentication.getAuthorities());
            return jwtService.generateTokens(authentication.getName(), role);
        } else if (user.getGrantType().equals("refreshToken") || user.getGrantType().equals("refresh-Token")){
            log.info("Raffraichissement de token pour {}", user.getRefreshToken());
            return jwtService.generateFromRefreshToken(user);
        } else{
            log.error("Utilisateur non identiufié : {}", user);
            return null;
        }
        
        

    }
}
