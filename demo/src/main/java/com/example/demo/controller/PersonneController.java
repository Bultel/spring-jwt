package com.example.demo.controller;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.demo.model.Personne;
import com.example.demo.service.PersonneService;

import jakarta.annotation.security.RolesAllowed;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin("http://127.0.0.1:5500") //authorise le front sur le port 5500 de live server
@RequestMapping("/personnes") //Route general
@RestController //Controller rest qui retourne pas de vue
@Slf4j //Journalisation
@AllArgsConstructor //Remplace l'autowired recommandé par spring
@Secured({"SCOPE_ADMIN"})  // Par defaut sur toutes les méthodes sans annotation - secured provient de spring
//@RolesAllowed({"ADMIN"}) // meme fonctionnalité que @Securied mais provient de Java
public class PersonneController {
    private PersonneService personneService;


    //#####################################GET################################
    //@Secured("ROLE_USER")
    @GetMapping()
    public List<Personne> getPersonnes () {
        log.info("liste de personnes consultée");
        System.out.println("Je suis la");
        return personneService.findAll();
    }

    //################################GET BY ID##############################
    @GetMapping("/{id}")
    // Avec la gestion d'erreur avec un message perso
    public ResponseEntity<Personne> getPersonnesById (@PathVariable int id) {
        log.info("personne rechéerché avec id {} ");
        var personne = personneService.findById(id);
        if (personne == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "La personne recherchée n'éxiste pas");
        }
        return new ResponseEntity<Personne>(personne, HttpStatus.OK);
    }

    //############################AJOUT ET SAUVEGARDE##############################@
    //@PreAuthorize("hasRole('ROLE_ADMIN') and hasRole('ROLE_USER')") // il faut le role admin ET user 
    //@Secured("ROLE_ADMIN")
    @PostMapping()
    @ResponseStatus(code = HttpStatus.CREATED) //Permet de changer le code serveur
    public Personne saveOrUpdatePersonne (@RequestBody Personne personne) {
        log.info("personne ajoutée avec id {} ");
        return personneService.save(personne);
    }

    // #############################DELETE#############################@
    @DeleteMapping("/delete/{id}")
    // Avec la gestion d'erreur avec un message perso
    public ResponseEntity<Boolean> deletePersonne (@PathVariable int id){

        // Recupere une personne
        var personne = personneService.findById(id);
        // Verifie si elle existe, si non, envoi un message d'erreur
        if (personne == null) {
            log.info("personne avec id {} est introuvable", id);
            return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
        }
        // Personne bien supprimer, envoi le bon code serveur
        personneService.deleteById(id);
        log.info("personne supprimé avec id {} ", id);
        return new ResponseEntity<Boolean>(true, HttpStatus.NO_CONTENT);
    }

    //################################UPDATE BY ID##################################
    @PutMapping("/{id}")
    // Avec la gestion d'erreur avec un message perso
    public ResponseEntity<?> updatePersonne (@RequestBody Personne personne, @PathVariable int id){

        if(id != personne.getNum()){
            log.info("Requete incoherente");
            return new ResponseEntity<Personne>(personne, HttpStatus.BAD_REQUEST);
        } else if (personneService.findById(id) == null){
            log.info("Requete incoherente");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

       return new ResponseEntity<Personne>(personneService.save(personne), HttpStatus.ACCEPTED);
    }

}
