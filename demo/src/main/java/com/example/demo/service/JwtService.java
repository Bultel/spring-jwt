package com.example.demo.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import com.example.demo.dto.UserDto;
import com.nimbusds.jwt.JWTClaimsSet;

import lombok.AllArgsConstructor;
import lombok.var;

@Service
@AllArgsConstructor
public class JwtService {

    private JwtEncoder jwtEncoder;
    private JwtDecoder jwtDecoder;
    private UserDetailsServiceImpl userDetailsServiceImpl;

    // ACCES TOKEN
    public Map<String, String> generateToken(String username, String roles){
        // Defini notre token
        JwtClaimsSet jwtClaimsSet = JwtClaimsSet.builder()
            .issuedAt(Instant.now())
            .expiresAt(Instant.now().plus(1, ChronoUnit.MINUTES)) // Durer de vie du jeton de 5 min
            .issuer("spring-secutity-oauth") // Optionnel on met ce que l'on veut
            .subject(username) // Ajoute le nom du user dans le token
            .claim("scope", roles) // Ajoute les roles
            .build();

        // Signature du token
        String token = jwtEncoder.encode(JwtEncoderParameters.from(jwtClaimsSet)).getTokenValue();

        // Stocke dans une MAP
        Map<String, String> idToken = new HashMap<>();
        idToken.put("access-token", token);

        return idToken;
    }

    //REFRESH TOKEN
    public Map<String, String> generateTokens(String username, String roles){
        var idToken = generateToken(username, roles);

        JwtClaimsSet jwtClaimsSet = JwtClaimsSet.builder()
        .issuedAt(Instant.now())
        .expiresAt(Instant.now().plus(1, ChronoUnit.DAYS)) // Durer de vie du jeton de 1 mois
        .issuer("spring-secutity-oauth") // Optionnel on met ce que l'on veut
        .subject(username) // Ajoute le nom du user dans le token
        .build();

        String token = jwtEncoder.encode(JwtEncoderParameters.from(jwtClaimsSet)).getTokenValue();
        idToken.put("refresh-token", token);

        return idToken;
    }

    public Map<String, String> generateFromRefreshToken(UserDto user) {
        var decodedJwt = jwtDecoder.decode(user.getRefreshToken());
        var nom = decodedJwt.getSubject();
        var connected = userDetailsServiceImpl.loadUserByUsername(nom);
        var roles = connected.getAuthorities().stream()
        //.map(elt -> elt.getAuthority()) Avec une lanmdba
        .map(GrantedAuthority::getAuthority) // Avec une ref de methode (destructuring)
        .collect(Collectors.joining(" "));

        return generateTokens(nom, roles);
    }

    public String getRoles(Collection<? extends GrantedAuthority> authorities) {
        var roles = authorities.stream()
        .map(elt -> elt.getAuthority())
        .collect(Collectors.joining(" "));
        return roles;
    }
    
}
